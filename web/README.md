# Overview

This is the main repository for the humansincluded web app. This web app is primarily written on top of the new https://material.io/components/web/ with an underlying structure copying that of the https://developers.google.com/web/tools/starter-kit/. Vue is the primary app framework.

# Installing

cd web
npm install

# Building

gulp

# Serving

gulp serve

# Testing

npm test