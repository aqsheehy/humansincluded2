export default class Modal {

    constructor(modal) { this.ref = modal; }

    open() {
        var modal = this.ref;
        if (!modal.open) modal.showModal();
        var close = modal.querySelector('.close');
        var submit = modal.querySelector('button[type="submit"]')

        return new Promise((resolve, reject) => {

            submit.addEventListener('click', function(event) {
                event.preventDefault();
                resolve(modal);
                return false;
            }, { once: true });

            close.addEventListener('click', function(event) {
                event.preventDefault();
                reject(modal);
                if (modal.open) modal.close();
            }, { once: true });

        });
    }
}